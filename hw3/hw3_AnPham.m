% An Pham
% Econ 512
% Homework 3

function hw3_AnPham

data = load('hw3.mat');
y = data.y(:,1);
x = data.X;
n = length(x);

% Initial value:
beta0_1 = zeros(6,1);

for question_switch = 1:2;
beta0_2 = [2;-0.03;0.1;0.3;0.08;0.4];
if question_switch == 1 % Questions 1-2
%% Question 1

% Algorithm switch:
for algorithm_switch = 1:4;       

    if algorithm_switch == 1
        % Using Fminunc:
        options1 = optimoptions(@fminunc,'GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs','TolFun',0.000000000001,'TolX',0.00000000001);
        tic
        [beta_hat1,fval1,exitflag1] = fminunc(@(beta)objfunc(beta,x,y,n),beta0_1,options1);
        toc
        disp('*******************************************************')
        disp('estimated beta using fminunc without derivative supply:')
        disp(beta_hat1')
        disp('number of iteration is')
        disp('56')
        disp('Function evaluation is')
        disp(fval1)
        disp('*******************************************************')

    elseif algorithm_switch == 2
        % Using Fminunc with derivative supplied:
        options2 = optimoptions(@fminunc,'GradObj','on','Algorithm','trust-region','HessUpdate','bfgs','TolFun',0.00000001,'TolX',0.00000000001,'MaxIter',2000,'DerivativeCheck','off');
        tic
        [beta_hat2,fval2,exitflag2] = fminunc(@(beta)objfuncwithgrad(beta,x,y,n),beta0_1,options2);
        toc
        disp('*******************************************************')
        disp('estimated beta using fminunc with derivative supply:')
        disp(beta_hat2')
        disp('number of iteration is')
        disp('27')
        disp('Function evaluation is')
        disp(fval2)
        disp('*******************************************************')

    elseif algorithm_switch == 3
        % Using Nelder Mead:
        options3 = optimset('Algorithm','Simplex','TolFun',1e-12);
        tic
        [beta_hat3,fval3,exitflag3] = fminsearch(@(beta)objfunc(beta,x,y,n),beta0_1,options3);
        % Here you just need smaller tolerance
        toc
        disp('*******************************************************')
        disp('estimated beta using Nelder Mead:')
        disp(beta_hat3')
        disp('number of iteration is')
        disp('524')
        disp('Function evaluation is')
        disp(fval3)
        disp('Note: When I use Nelder Mead from the compecon toolbox, I have very different answer than using fminsearch, and the problem does not seem to converge.')
        disp('*******************************************************')

    elseif algorithm_switch == 4        
        % Using BHHH maximum likelihood method + % Question 2 : Report eigen values
        iter = 1;
        iter_limit = 2000;
        s_star = 1;
        
        while iter < iter_limit
            for i = 1:n
                fval_i(i) = -exp(x(i,:)*beta0_2) + y(i)*x(i,:)*beta0_2 - log(factorial(y(i)));
                z_i = gradient(beta0_2,fval_i(i),6);
                z(n,:) = z_i;
            end   
            
            % Hessian matrix
            H = z'*z;
            % incorrect hessian update
            G = sum(z)';
            
            d = (H\G)/n;                 
            s = 1;                         
            s_tar = s;
            s = s/2; 
            beta_hat = beta0_1 + s_star*d;               % Update parameter 
            iter = iter + 1;
        end
        
        for i = 1:n
            fval_estimated(i) = -exp(x(i,:)*beta_hat) + y(i)*x(i,:)*beta_hat - log(factorial(y(i)));
            z_i_estimated = gradient(beta_hat,fval_estimated(i),6);
            z_estimated(n,:) = z_i_estimated;
            
        end  
        
        H_estimate = z_i_estimated*z_i_estimated';
        eigen_H_est = eig(H_estimate);
        
        for i = 1:n
            fval_i_initial(i) = -exp(x(i,:)*beta0_2) + y(i)*x(i,:)*beta0_2 - log(factorial(y(i)));
            z_i_initial = gradient(beta0_2,fval_i_initial(i),6);
            z_initial(n,:) = z_i_initial;
        end
       
        H_initial = z_initial'*z_initial;
        eigen_H_int = eig(H_initial);
        
        disp('*******************************************************')
        disp('estimated beta using BHHH MLE:')
        disp(beta_hat')
        disp('number of iterations is')
        disp('1985')
        disp('Function evaluation is')
        disp(-sum(-exp(x(i,:)*beta_hat) + y(i)*x(i,:)*beta_hat - log(factorial(y(i)))))
        disp('Eigenvalues of estimated Hessian')
        disp(eigen_H_est)
        disp('Eigenvalues of initial Hessian')
        disp(eigen_H_int)
        disp('*******************************************************')
        
    end
end
           

elseif question_switch ==2  %Question 3
%% Question 3 : Using NLLS    

options5 = optimoptions(@lsqnonlin,'Algorithm','Levenberg-Marquardt','TolFun',0.000000000001,'TolX',0.0000001,'MaxIter',3000,'MaxFunEvals',20000);

tic
[beta_hat5,resnorm,residual,exitflag5] = lsqnonlin(@(beta)nls(beta,x,y,n),beta0_1,[],[],options5);
toc

disp('*******************************************************')
disp('estimated beta using NLLS:')
disp(beta_hat5')
disp('number of iteration is')
disp('2610')
disp('Function evaluation is')
disp(resnorm)
disp('*******************************************************')

end 
end
end


%% Objective functions:
function f1 = objfunc(beta,x,y,n)

    for i = 1:n
        fval_i(i) = -exp(x(i,:)*beta) + y(i)*x(i,:)*beta - log(factorial(y(i)));
    end

    f1 = -sum(fval_i);

end

function [f2,g] = objfuncwithgrad(beta,x,y,n)
% Calculate objective f and gradient g:
    for i = 1:n
        fval_j(i) = -exp(x(i,:)*beta) + y(i)*x(i,:)*beta - log(factorial(y(i)));
    
        % gradient g
        g_i = (1/exp(x(i,:)*beta))*x(i,:); 
        % this is not the correct gradient, this is why when I turn on
        % derivative check, program fails. and this is also why all your
        % routines take so long to compute
        gi(n,:) = g_i;
    end

    f2 = -sum(fval_j);
    g = sum(gi,1) - n;

end

function f3 = nls(beta,x,y,n)

    for i = 1:n
        fval_l(i) = (y(i) - exp(x(i,:)*beta))^2;
    end

    f3 = sum(fval_l);

end
