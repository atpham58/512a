diary hw1_AP.out


% An Pham
% Econ 512A
% Homework 1

%% Question 1: 
X = [1, 1.5, 3.5, 4.78, 5, 7.2, 9, 10, 10.1, 10.56]; % Given vector
Y1 = -2 + 0.5*X; 
Y2 = -2 + 0.5*X.^2;
plot(X,Y1,'g',X,Y2,'b');
ylabel('Y_i');
xlabel('X');
legend('Y1','Y2','Location','northeast')

%% Question 2: Create a 120x1 vector X containing evenly-spaced numbers starting between 20 and 40. Calculate the sum of the elements of the vector
X = linspace(-20,40,120);
X
sum(X)

%% Question 3: 
A = [3,4,5;2,12,6;1,7,4];
b = [3;-2;10];

C = A'*b;
C
D = (inv(A'*A))*b;
D
E = 0;
for i = 1:3
    for j = 1:3
        E = sum(A*b);
    end
end
% Same mistake Joseph Perla made. See answer keys
E

F = A([1,3],[1,2]);
F

% Solve the system of linear equations Ax = b for the vector x 
x = linsolve(A,b);
x

%% Question 4:
O = zeros(3,3);
B = [A,O,O,O,O;O,A,O,O,O;O,O,A,O,O;O,O,O,A,O;O,O,O,O,A];
B

%% Question 5:
A = normrnd(6.5,3,[5,3]);
Z = A;
for i=1:5
    for j=1:3
        if Z(i,j)<4
            Z(i,j)=0;
        elseif Z(i,j)>04
            Z(i,j)=1;
        end
    end
end

A
Z

%% Question 6:
data = csvread('datahw1.csv');
% comment from TA: csvread replaces NaN with 0 which makes your dataset corrupted. use readtable instead.

firm_ID = data(:,1);
year = data(:,2);
dummy1 = data(:,3);
dummy2 = data(:,4);
prod = data(:,5);
cap = data(:,6);

xdata = [dummy1,dummy2,cap];
ydata = prod;

mdl = fitlm(xdata,ydata);

% Report the estimates:
mdl.Coefficients(:,1)

diary off
